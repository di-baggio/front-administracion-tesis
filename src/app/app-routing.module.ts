import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './components/admin/admin.component';
import { CategoriaComponent } from './components/categoria/categoria.component';
import { CiudadComponent } from './components/ciudad/ciudad.component';
import { DepartamentoComponent } from './components/departamento/departamento.component';
import { LoginComponent } from './components/login/login.component';
import { PaisComponent } from './components/pais/pais.component';
import { RegimenTributarioComponent } from './components/regimen-tributario/regimen-tributario.component';
import { ServicioComponent } from './components/servicio/servicio.component';
import { CheckLoginAppGuard } from './components/shared/guards/check-login-app.guard';
import { CheckLoginGuard } from './components/shared/guards/check-login.guard';
import { TipoDocumentoComponent } from './components/tipo-documento/tipo-documento.component';
import { TipoSolicitudComponent } from './components/tipo-solicitud/tipo-solicitud.component';
import { UsuarioComponent } from './components/usuario/usuario.component';


const routes: Routes = [{
  path: 'admin',
  component: AdminComponent,
  canActivate: [CheckLoginAppGuard],
  children: [{
    path: 'pais',
    component: PaisComponent,
    canActivate: [CheckLoginAppGuard]
  }, {
    path: 'tiposolicitud',
    component: TipoSolicitudComponent,
    canActivate: [CheckLoginAppGuard]
  }, {
    path: 'tipodocumento',
    component: TipoDocumentoComponent,
    canActivate: [CheckLoginAppGuard]
  }, {
    path: 'regimentributario',
    component: RegimenTributarioComponent,
    canActivate: [CheckLoginAppGuard]
  }, {
    path: 'departamento',
    component: DepartamentoComponent,
    canActivate: [CheckLoginAppGuard]
  }, {
    path: 'categoria',
    component: CategoriaComponent,
    canActivate: [CheckLoginAppGuard]
  }, {
    path: 'servicio',
    component: ServicioComponent,
    canActivate: [CheckLoginAppGuard]
  },
  {
    path: 'ciudad',
    component: CiudadComponent,
    canActivate: [CheckLoginAppGuard]
  }, {
    path: 'usuario',
    component: UsuarioComponent,
    canActivate: [CheckLoginAppGuard]
  }]
}, {
  path: 'login',
  component: LoginComponent,
  canActivate: [CheckLoginGuard]
},
{
  path: '**',
  redirectTo: 'login'
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import {
  DxDataGridModule,
  DxButtonModule,
  DxSwitchModule,
  DxSelectBoxModule,
  DxDateBoxModule,
} from 'devextreme-angular';


import { HeaderComponent } from './components/shared/header/header.component';
import { MenuComponent } from './components/shared/menu/menu.component';
import { FooterComponent } from './components/shared/footer/footer.component';
import { SettingComponent } from './components/shared/setting/setting.component';
import { AdminComponent } from './components/admin/admin.component';
import { LoginModule } from './components/login/login.module';
import { PaisComponent } from './components/pais/pais.component';
import { TipoSolicitudComponent } from './components/tipo-solicitud/tipo-solicitud.component';
import { TipoDocumentoComponent } from './components/tipo-documento/tipo-documento.component';
import { RegimenTributarioComponent } from './components/regimen-tributario/regimen-tributario.component';
import { DepartamentoComponent } from './components/departamento/departamento.component';
import { CategoriaComponent } from './components/categoria/categoria.component';
import { ServicioComponent } from './components/servicio/servicio.component';
import { CiudadComponent } from './components/ciudad/ciudad.component';
import { UsuarioComponent } from './components/usuario/usuario.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MenuComponent,
    FooterComponent,
    SettingComponent,
    AdminComponent,
    PaisComponent,
    TipoSolicitudComponent,
    TipoDocumentoComponent,
    RegimenTributarioComponent,
    DepartamentoComponent,
    CategoriaComponent,
    ServicioComponent,
    CiudadComponent,
    UsuarioComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    LoginModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    DxDataGridModule,
    DxButtonModule,
    DxSwitchModule,
    DxSelectBoxModule,
    DxDateBoxModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

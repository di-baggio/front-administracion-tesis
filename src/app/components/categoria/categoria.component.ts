import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { DxDataGridComponent } from 'devextreme-angular';
import { CategoriaService } from './categoria.service';
declare var $: any;
@Component({
  selector: 'app-categoria',
  templateUrl: './categoria.component.html',
  styleUrls: ['./categoria.component.scss']
})
export class CategoriaComponent implements OnInit {
  listCategoria: any[] = [];
  Estados: any[] = [{ estado: 0, nombre: 'INACTIVO' }, { estado: 1, nombre: 'ACTIVO' }];

  @ViewChild('gridCategoria', { static: true }) gridCategoria: DxDataGridComponent;
  isNew = false;
  CategoriaForm = this.fb.group({
    id: [0],
    nombre: [''],
    estado: [-1]
  });
  constructor(private categoriaService: CategoriaService, private fb: FormBuilder) { this.Editar = this.Editar.bind(this); }


  async ngOnInit() {
    await this.listarCategoria();
  }
  async listarCategoria() {
    await this.categoriaService.listarCategoria({}).subscribe(res => {
      this.listCategoria = res;
    });
  }
  Editar(e) {
    this.isNew = false;
    const data = e.row.data;
    this.CategoriaForm.setValue({
      id: e.row.data.id,
      nombre: e.row.data.nombre,
      estado: e.row.data.estado
    });
    $('#ModalActualizarEditar').modal('show');
  }
  abrirModal() {
    this.isNew = true;
    $('#ModalActualizarEditar').modal('show');
  }
  cancelarModal() {
    $('#ModalActualizarEditar').modal('hide');
    this.limpiarForm();
  }
  onCreateCategoria() {
    const formValue = this.CategoriaForm.value;

    if (this.isNew) {
      this.categoriaService.CreateCategoria({ nombre: formValue.nombre, estado: Number(formValue.estado) }).subscribe(async res => {
        await this.listarCategoria();
      });
    } else {
      this.categoriaService.UpdateCategoria({ id: formValue.id, nombre: formValue.nombre, estado: Number(formValue.estado) }
      ).subscribe(async res => {
        await this.listarCategoria();
      });
    }
    $('#ModalActualizarEditar').modal('hide');
    this.limpiarForm();
  }
  limpiarForm() {
    this.CategoriaForm = this.fb.group({
      id: [0],
      nombre: [''],
      estado: [-1]
    });
  }

}

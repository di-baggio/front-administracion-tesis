import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Categoria } from '../shared/models/categoria.interface';

@Injectable({
  providedIn: 'root'
})
export class CategoriaService {
  headersObject: HttpHeaders;
  httpOptions: any;
  constructor(private http: HttpClient, private toastr: ToastrService) {
    this.headersObject = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('token'));
    this.httpOptions = {
      headers: this.headersObject
    };
  }
  listarCategoria(data: any): Observable<Categoria[]> {

    return this.http.get(`${environment.APIURL}/Categoria${(data.id ? '?id=' + data.id : '')}`, this.httpOptions).pipe(
      map((res: any) => {
        const Categoriaes: Categoria[] = res.data;
        return Categoriaes;
      }),
      catchError((err: any) => this.handleError(err))
    );
  }

  UpdateCategoria(data: any): Observable<any> {

    return this.http.put(`${environment.APIURL}/Categoria/${data.id}`, data, this.httpOptions).pipe(
      map((res: any) => {
        this.toastr.success(res.message);
        return res;
      }),
      catchError((err: any) => this.handleError(err))
    );
  }
  CreateCategoria(data: any): Observable<any> {
    return this.http.post(`${environment.APIURL}/Categoria`, data, this.httpOptions).pipe(
      map((res: any) => {
        this.toastr.success(res.message);
        return res;
      }),
      catchError((err: any) => this.handleError(err))
    );
  }

  handleError(err: any): Observable<never> {
    if (err) {
      let errorMessage: any;
      if (typeof err.error.error === 'object') {
        errorMessage = err.error.error.message;
        this.toastr.error(err.error.error.message);
      } else {
        errorMessage = err.error.message;
        this.toastr.error(err.error.message);
      }
      return errorMessage;
    }
    return err;
  }
}

import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { DxDataGridComponent } from 'devextreme-angular';
import { DepartamentoService } from '../departamento/departamento.service';
import { CiudadService } from './ciudad.service';

declare var $: any;
@Component({
  selector: 'app-ciudad',
  templateUrl: './ciudad.component.html',
  styleUrls: ['./ciudad.component.scss']
})
export class CiudadComponent implements OnInit {

  listCiudad: any[] = [];
  listDepartamentos: any[] = [];

  Estados: any[] = [{ estado: 0, nombre: 'INACTIVO' }, { estado: 1, nombre: 'ACTIVO' }];

  @ViewChild('gridCiudad', { static: true }) gridCiudad: DxDataGridComponent;
  isNew = false;
  CiudadesForm = this.fb.group({
    id: [0],
    id_departamento: [1],
    codigo: [''],
    nombre: [''],
    estado: [-1]
  });
  constructor(private ciudadService: CiudadService, private fb: FormBuilder, private departamentoService: DepartamentoService) {
    this.Editar = this.Editar.bind(this);
  }

  async ngOnInit() {
    await this.listarCiudad();
    await this.listarDepartamentos();
  }
  async listarCiudad() {
    await this.ciudadService.listarCiudad({}).subscribe(res => {
      this.listCiudad = res;
    });
  }
  async listarDepartamentos() {
    await this.departamentoService.listarDepartamento({}).subscribe(res => {
      this.listDepartamentos = res;
    });
  }
  Editar(e) {
    this.isNew = false;
    this.CiudadesForm.setValue({
      id: e.row.data.id,
      id_departamento: e.row.data.id_departamento,
      codigo: e.row.data.codigo,
      nombre: e.row.data.nombre,
      estado: e.row.data.estado
    });
    $('#ModalActualizarEditar').modal('show');
  }
  abrirModal() {
    this.isNew = true;
    $('#ModalActualizarEditar').modal('show');
  }
  cancelarModal() {
    $('#ModalActualizarEditar').modal('hide');
    this.limpiarForm();
  }
  onCreateCiudad() {
    const formValue = this.CiudadesForm.value;

    if (this.isNew) {
      this.ciudadService.CreateCiudad({
        id_departamento: formValue.id_departamento,
        codigo: formValue.codigo,
        nombre: formValue.nombre,
        estado: Number(formValue.estado)
      }).subscribe(async res => {
        await this.listarCiudad();
      });
    } else {
      this.ciudadService.UpdateCiudad({
        id: formValue.id,
        id_departamento: formValue.id_departamento,
        codigo: formValue.codigo,
        nombre: formValue.nombre,
        estado: Number(formValue.estado)
      }
      ).subscribe(async res => {
        await this.listarCiudad();
      });
    }
    $('#ModalActualizarEditar').modal('hide');
    this.limpiarForm();
  }
  limpiarForm() {
    this.CiudadesForm = this.fb.group({
      id: [0],
      id_departamento: [1],
      codigo: [''],
      nombre: [''],
      estado: [-1]
    });
  }

}

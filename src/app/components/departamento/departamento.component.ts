import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { DxDataGridComponent } from 'devextreme-angular';
import { PaisService } from '../pais/pais.service';
import { DepartamentoService } from './departamento.service';

declare var $: any;
@Component({
  selector: 'app-departamento',
  templateUrl: './departamento.component.html',
  styleUrls: ['./departamento.component.scss']
})
export class DepartamentoComponent implements OnInit {

  listDepartamento: any[] = [];
  listPaises: any[] = [];

  Estados: any[] = [{ estado: 0, nombre: 'INACTIVO' }, { estado: 1, nombre: 'ACTIVO' }];

  @ViewChild('gridDepartamento', { static: true }) gridDepartamento: DxDataGridComponent;
  isNew = false;
  DepartamentoForm = this.fb.group({
    id: [0],
    id_pais: [1],
    nombre: [''],
    estado: [-1]
  });
  constructor(private departamentoService: DepartamentoService, private fb: FormBuilder, private paisService: PaisService) {
    this.Editar = this.Editar.bind(this);
  }

  async ngOnInit() {
    await this.listarDepartamento();
    await this.listarPaises();
  }
  async listarDepartamento() {
    await this.departamentoService.listarDepartamento({}).subscribe(res => {
      this.listDepartamento = res;
    });
  }
  async listarPaises() {
    await this.paisService.listarPaises({}).subscribe(res => {
      this.listPaises = res;
    });
  }
  Editar(e) {
    this.isNew = false;
    this.DepartamentoForm.setValue({
      id: e.row.data.id,
      id_pais: e.row.data.id_pais,
      nombre: e.row.data.nombre,
      estado: e.row.data.estado
    });
    $('#ModalActualizarEditar').modal('show');
  }
  abrirModal() {
    this.isNew = true;
    $('#ModalActualizarEditar').modal('show');
  }
  cancelarModal() {
    $('#ModalActualizarEditar').modal('hide');
    this.limpiarForm();
  }
  onCreateDepartamento() {
    const formValue = this.DepartamentoForm.value;

    if (this.isNew) {
      this.departamentoService.CreateDepartamento({
        id_pais: formValue.id_pais,
        nombre: formValue.nombre,
        estado: Number(formValue.estado)
      }).subscribe(async res => {
        await this.listarDepartamento();
      });
    } else {
      this.departamentoService.UpdateDepartamento({
        id: formValue.id,
        id_pais: formValue.id_pais,
        nombre: formValue.nombre,
        estado: Number(formValue.estado)
      }
      ).subscribe(async res => {
        await this.listarDepartamento();
      });
    }
    $('#ModalActualizarEditar').modal('hide');
    this.limpiarForm();
  }
  limpiarForm() {
    this.DepartamentoForm = this.fb.group({
      id: [0],
      id_pais: [1],
      nombre: [''],
      estado: [-1]
    });
  }


}

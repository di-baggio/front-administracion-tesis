import { Component, OnDestroy, OnInit } from '@angular/core';
import { LoginService } from './login.service';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  subscriptions: Subscription;
  loginForm = this.fb.group({
    username: [''],
    password: ['']
  });

  constructor(private loginService: LoginService, private fb: FormBuilder, private router: Router) { }

  ngOnInit() {
  }
  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }
  onLogin() {
    const formValue = this.loginForm.value;
    this.subscriptions = this.loginService.login(formValue).subscribe(res => {
      if (res) {
        this.router.navigate(['admin']);
      }
    });
  }
}

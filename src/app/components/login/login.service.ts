import { Injectable, resolveForwardRef } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { User, UserResponse } from '../shared/models/user.interface';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

const helpter = new JwtHelperService();

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private logggedIn = new BehaviorSubject<boolean>(false);
  public usuario: UserResponse;
  constructor(private http: HttpClient, private router: Router, private toastr: ToastrService) {
    this.readToken();
  }
  getIsLogged(): Observable<boolean> {
    return this.logggedIn.asObservable();
  }
  login(authData: User): Observable<UserResponse | void> {
    return this.http.post<UserResponse>(`${environment.APIURL}/login`, authData).pipe(
      map((res: UserResponse) => {
        if (res.usuario.id_tipo_usuario === 3) {
          this.saveToken(res.token);
          this.logggedIn.next(true);
          this.usuario = res;
        } else {
          throw { error: { error: { message: 'Usuario no autorizado' } } };
        }
        return res;

      }),
      catchError((err: any) => this.handleError(err))
    );
  }
  logout(): void {
    localStorage.removeItem('token');
    this.logggedIn.next(false);
    this.router.navigate(['login']);
  }
  private readToken(): void {
    const userToken = localStorage.getItem('token');
    const isExpired = helpter.isTokenExpired(userToken);
    if (isExpired) {
      this.logout();
    } else {
      this.logggedIn.next(true);
    }
  }
  private saveToken(token: string): void {
    localStorage.setItem('token', token);
  }

  private handleError(err: any): Observable<never> {
    let errorMessage = 'An error occurred ';
    if (err) {
      errorMessage = err.error.error.message;
      this.toastr.error(err.error.error.message);
    }
    return throwError(errorMessage);
  }
}

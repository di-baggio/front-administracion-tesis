import { Component, OnInit, ViewChild } from '@angular/core';
import { DxDataGridComponent } from 'devextreme-angular';
import { FormBuilder } from '@angular/forms';
import { Pais } from '../shared/models/pais.interface';
import { PaisService } from './pais.service';
declare var $: any;
@Component({
  selector: 'app-pais',
  templateUrl: './pais.component.html',
  styleUrls: ['./pais.component.scss']
})
export class PaisComponent implements OnInit {
  listPaises: Pais[] = [];
  Estados: any[] = [{ estado: 0, nombre: 'INACTIVO' }, { estado: 1, nombre: 'ACTIVO' }];

  @ViewChild('gridPaises', { static: true }) gridPaises: DxDataGridComponent;
  isNew = false;
  paisForm = this.fb.group({
    id: [0],
    nombre: [''],
    estado: [-1]
  });

  constructor(private paisService: PaisService, private fb: FormBuilder) {
    this.Editar = this.Editar.bind(this);
  }

  async ngOnInit() {
    await this.listarPaises();
  }
  async listarPaises() {
    await this.paisService.listarPaises({}).subscribe(res => {
      this.listPaises = res;
    });
  }
  Editar(e) {
    this.isNew = false;
    const data = e.row.data;
    this.paisForm.setValue({
      id: e.row.data.id,
      nombre: e.row.data.nombre,
      estado: e.row.data.estado
    });
    $('#ModalActualizarEditar').modal('show');
  }
  abrirModal() {
    this.isNew = true;
    $('#ModalActualizarEditar').modal('show');
  }
  cancelarModal() {
    $('#ModalActualizarEditar').modal('hide');
    this.limpiarForm();
  }
  onCreatePais() {
    const formValue = this.paisForm.value;

    if (this.isNew) {
      this.paisService.CreatePaises({ nombre: formValue.nombre, estado: Number(formValue.estado) }).subscribe(async res => {
        await this.listarPaises();
      });
    } else {
      this.paisService.UpdatePaises({ id: formValue.id, nombre: formValue.nombre, estado: Number(formValue.estado) }
      ).subscribe(async res => {
        await this.listarPaises();
      });
    }
    $('#ModalActualizarEditar').modal('hide');
    this.limpiarForm();
  }
  limpiarForm() {
    this.paisForm = this.fb.group({
      id: [0],
      nombre: [''],
      estado: [-1]
    });
  }
}

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';


import { environment } from '../../../environments/environment';
import { Pais } from '../shared/models/pais.interface';
@Injectable({
  providedIn: 'root'
})
export class PaisService {
  headersObject: HttpHeaders;
  httpOptions: any;
  constructor(private http: HttpClient, private toastr: ToastrService) {
    this.headersObject = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('token'));
    this.httpOptions = {
      headers: this.headersObject
    };
  }

  listarPaises(data: any): Observable<Pais[]> {

    return this.http.get(`${environment.APIURL}/pais${(data.id ? '?id=' + data.id : '')}`, this.httpOptions).pipe(
      map((res: any) => {
        const paises: Pais[] = res.data;
        return paises;
      }),
      catchError((err: any) => this.handleError(err))
    );
  }

  UpdatePaises(data: any): Observable<any> {

    return this.http.put(`${environment.APIURL}/pais/${data.id}`, data, this.httpOptions).pipe(
      map((res: any) => {
        this.toastr.success(res.message);
        return res;
      }),
      catchError((err: any) => this.handleError(err))
    );
  }
  CreatePaises(data: any): Observable<any> {
    return this.http.post(`${environment.APIURL}/pais`, data, this.httpOptions).pipe(
      map((res: any) => {
        this.toastr.success(res.message);
        return res;
      }),
      catchError((err: any) => this.handleError(err))
    );
  }

  handleError(err: any): Observable<never> {
    if (err) {
      let errorMessage: any;
      if (typeof err.error.error === 'object') {
        errorMessage = err.error.error.message;
        this.toastr.error(err.error.error.message);
      } else {
        errorMessage = err.error.message;
        this.toastr.error(err.error.message);
      }
      return errorMessage;
    }
    return err;
  }
}

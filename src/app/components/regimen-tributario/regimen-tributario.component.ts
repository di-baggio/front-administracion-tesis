import { ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { DxDataGridComponent } from 'devextreme-angular';
import { RegimenTributarioService } from './regimen-tributario.service';
declare var $: any;
@Component({
  selector: 'app-regimen-tributario',
  templateUrl: './regimen-tributario.component.html',
  styleUrls: ['./regimen-tributario.component.scss']
})
export class RegimenTributarioComponent implements OnInit {
  listRegimenTributario: any[] = [];
  Estados: any[] = [{ estado: 0, nombre: 'INACTIVO' }, { estado: 1, nombre: 'ACTIVO' }];

  @ViewChild('gridRegimenTributario', { static: true }) gridRegimenTributario: DxDataGridComponent;
  isNew = false;
  RegimenTributarioForm = this.fb.group({
    id: [0],
    codigo: [''],
    nombre: [''],
    estado: [-1]
  });
  constructor(private regimenTributarioService: RegimenTributarioService, private fb: FormBuilder) { this.Editar = this.Editar.bind(this); }


  async ngOnInit() {
    await this.listarRegimenTributario();
  }
  async listarRegimenTributario() {
    await this.regimenTributarioService.listarRegimenTributario({}).subscribe(res => {
      this.listRegimenTributario = res;
    });
  }
  Editar(e) {
    this.isNew = false;
    const data = e.row.data;
    this.RegimenTributarioForm.setValue({
      id: e.row.data.id,
      codigo: e.row.data.codigo,
      nombre: e.row.data.nombre,
      estado: e.row.data.estado
    });
    $('#ModalActualizarEditar').modal('show');
  }
  abrirModal() {
    this.isNew = true;
    $('#ModalActualizarEditar').modal('show');
  }
  cancelarModal() {
    $('#ModalActualizarEditar').modal('hide');
    this.limpiarForm();
  }
  onCreateRegimenTributario() {
    const formValue = this.RegimenTributarioForm.value;

    if (this.isNew) {
      this.regimenTributarioService.CreateRegimenTributario({
        codigo: formValue.codigo,
        nombre: formValue.nombre,
        estado: Number(formValue.estado)
      }).subscribe(async res => {
        await this.listarRegimenTributario();
      });
    } else {
      this.regimenTributarioService.UpdateRegimenTributario({
        id: formValue.id,
        codigo: formValue.codigo,
        nombre: formValue.nombre,
        estado: Number(formValue.estado)
      }
      ).subscribe(async res => {
        await this.listarRegimenTributario();
      });
    }
    $('#ModalActualizarEditar').modal('hide');
    this.limpiarForm();
  }
  limpiarForm() {
    this.RegimenTributarioForm = this.fb.group({
      id: [0],
      codigo: [''],
      nombre: [''],
      estado: [-1]
    });
  }

}

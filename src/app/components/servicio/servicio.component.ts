import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { DxDataGridComponent } from 'devextreme-angular';
import { CategoriaService } from '../categoria/categoria.service';
import { ServicioService } from './servicio.service';

declare var $: any;
@Component({
  selector: 'app-servicio',
  templateUrl: './servicio.component.html',
  styleUrls: ['./servicio.component.scss']
})
export class ServicioComponent implements OnInit {


  listServicio: any[] = [];
  listCategorias: any[] = [];

  Estados: any[] = [{ estado: 0, nombre: 'INACTIVO' }, { estado: 1, nombre: 'ACTIVO' }];

  @ViewChild('gridServicio', { static: true }) gridServicio: DxDataGridComponent;
  isNew = false;
  ServicioForm = this.fb.group({
    id: [0],
    id_categoria: [1],
    nombre: [''],
    estado: [-1]
  });
  constructor(private servicioService: ServicioService, private fb: FormBuilder, private categoriaService: CategoriaService) {
    this.Editar = this.Editar.bind(this);
  }

  async ngOnInit() {
    await this.listarServicio();
    await this.listarCategoriaes();
  }
  async listarServicio() {
    await this.servicioService.listarServicio({}).subscribe(res => {
      this.listServicio = res;
    });
  }
  async listarCategoriaes() {
    await this.categoriaService.listarCategoria({}).subscribe(res => {
      this.listCategorias = res;
    });
  }
  Editar(e) {
    this.isNew = false;
    this.ServicioForm.setValue({
      id: e.row.data.id,
      id_categoria: e.row.data.id_categoria,
      nombre: e.row.data.nombre,
      estado: e.row.data.estado
    });
    $('#ModalActualizarEditar').modal('show');
  }
  abrirModal() {
    this.isNew = true;
    $('#ModalActualizarEditar').modal('show');
  }
  cancelarModal() {
    $('#ModalActualizarEditar').modal('hide');
    this.limpiarForm();
  }
  onCreateServicio() {
    const formValue = this.ServicioForm.value;

    if (this.isNew) {
      this.servicioService.CreateServicio({
        id_categoria: formValue.id_categoria,
        nombre: formValue.nombre,
        estado: Number(formValue.estado)
      }).subscribe(async res => {
        await this.listarServicio();
      });
    } else {
      this.servicioService.UpdateServicio({
        id: formValue.id,
        id_categoria: formValue.id_categoria,
        nombre: formValue.nombre,
        estado: Number(formValue.estado)
      }
      ).subscribe(async res => {
        await this.listarServicio();
      });
    }
    $('#ModalActualizarEditar').modal('hide');
    this.limpiarForm();
  }
  limpiarForm() {
    this.ServicioForm = this.fb.group({
      id: [0],
      id_categoria: [1],
      nombre: [''],
      estado: [-1]
    });
  }

}

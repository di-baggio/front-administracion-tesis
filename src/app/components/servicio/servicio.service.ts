import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Servicio } from '../shared/models/servicio.interface';

@Injectable({
  providedIn: 'root'
})
export class ServicioService {
  headersObject: HttpHeaders;
  httpOptions: any;
  constructor(private http: HttpClient, private toastr: ToastrService) {
    this.headersObject = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('token'));
    this.httpOptions = {
      headers: this.headersObject
    };
  }
  listarServicio(data: any): Observable<Servicio[]> {
    return this.http.get(`${environment.APIURL}/Servicio${(data.id ? '?id=' + data.id : '')}`, this.httpOptions).pipe(
      map((res: any) => {
        const servicio: Servicio[] = res.data;
        return servicio;
      }),
      catchError((err: any) => this.handleError(err))
    );
  }

  UpdateServicio(data: any): Observable<any> {
    return this.http.put(`${environment.APIURL}/Servicio/${data.id}`, data, this.httpOptions).pipe(
      map((res: any) => {
        this.toastr.success(res.message);
        return res;
      }),
      catchError((err: any) => this.handleError(err))
    );
  }
  CreateServicio(data: any): Observable<any> {
    return this.http.post(`${environment.APIURL}/Servicio`, data, this.httpOptions).pipe(
      map((res: any) => {
        this.toastr.success(res.message);
        return res;
      }),
      catchError((err: any) => this.handleError(err))
    );
  }

  handleError(err: any): Observable<never> {
    if (err) {
      let errorMessage: any;
      if (typeof err.error.error === 'object') {
        errorMessage = err.error.error.message;
        this.toastr.error(err.error.error.message);
      } else {
        errorMessage = err.error.message;
        this.toastr.error(err.error.message);
      }
      return errorMessage;
    }
    return err;
  }
}

import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { LoginService } from '../../login/login.service';

@Injectable({
  providedIn: 'root'
})
export class CheckLoginAppGuard implements CanActivate {
  constructor(private loginService: LoginService, private router: Router) { }
  canActivate() {
    return this.loginService.getIsLogged().pipe(
      take(1),
      map((isLogged: boolean) => {
        if (!isLogged) {
          return this.router.parseUrl('/login');
        } else {
          return isLogged;
        }
      })
    );
  }

}

import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../login/login.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html'
})
export class MenuComponent implements OnInit {
  nombreUsuario = 'Usuario';
  constructor(private loginService: LoginService) {
    if (this.loginService.usuario) {
      this.nombreUsuario = this.loginService.usuario.usuario.nombre;
    }
  }
  ngOnInit() {

  }

}

export interface Ciudad {
    id_pais: number;
    nombre_pais: string;
    id_departamento: number;
    nombre_departamento: string;
    id: number;
    codigo: string;
    nombre: string;
    estado: number;
}

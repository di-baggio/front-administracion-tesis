export interface Departamento {
    id_pais: number;
    nombre_pais: string;
    id: number;
    nombre: string;
    estado: number;
}

export interface RegimenTributario {
    id: number;
    codigo: string;
    nombre: string;
    estado: number;
}


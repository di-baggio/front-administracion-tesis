export interface Servicio {
    id_categoria: number;
    nombre_categoria: string;
    id: number;
    nombre: string;
    estado: number;
}

export interface User {
    username: string;
    password: string;
}

class UsersResponse {
    id: number;
    // tslint:disable-next-line: variable-name
    id_tipo_usuario: number;
    // tslint:disable-next-line: variable-name
    tipo_usuario: number;
    email: string;
    nombre: string;
    dni: string;
    telefono: string;
    estado: number;
}
export interface UserResponse {
    token: string;
    usuario: UsersResponse;
    fechaExpira: string;
}
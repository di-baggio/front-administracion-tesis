export interface Usuario {
    id: number;
    id_tercero: number;
    id_tipo_documento: number;
    documento: string;
    nombre: string;
    email: string;
    password: string;
    estado: number;
}

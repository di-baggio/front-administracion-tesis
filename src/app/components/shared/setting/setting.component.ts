import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../login/login.service';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html'
})
export class SettingComponent implements OnInit {

  constructor(private loginService: LoginService) { }

  ngOnInit() {
  }
  onLogout(): void {
    this.loginService.logout();
  }

}

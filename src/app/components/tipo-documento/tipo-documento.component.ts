import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { DxDataGridComponent } from 'devextreme-angular';
import { TipoDocumentoService } from './tipo-documento.service';
declare var $: any;
@Component({
  selector: 'app-tipo-documento',
  templateUrl: './tipo-documento.component.html',
  styleUrls: ['./tipo-documento.component.scss']
})
export class TipoDocumentoComponent implements OnInit {

  listTipoDocumento: any[] = [];
  Estados: any[] = [{ estado: 0, nombre: 'INACTIVO' }, { estado: 1, nombre: 'ACTIVO' }];

  @ViewChild('gridTipoDocumento', { static: true }) gridTipoDocumento: DxDataGridComponent;
  isNew = false;
  TipoDocumentoForm = this.fb.group({
    id: [0],
    nombre: [''],
    estado: [-1]
  });
  constructor(private tipoDocumentoService: TipoDocumentoService, private fb: FormBuilder) { this.Editar = this.Editar.bind(this); }


  async ngOnInit() {
    await this.listarTipoDocumento();
  }
  async listarTipoDocumento() {
    await this.tipoDocumentoService.listarTipoDocumento({}).subscribe(res => {
      this.listTipoDocumento = res;
    });
  }
  Editar(e) {
    this.isNew = false;
    const data = e.row.data;
    this.TipoDocumentoForm.setValue({
      id: e.row.data.id,
      nombre: e.row.data.nombre,
      estado: e.row.data.estado
    });
    $('#ModalActualizarEditar').modal('show');
  }
  abrirModal() {
    this.isNew = true;
    $('#ModalActualizarEditar').modal('show');
  }
  cancelarModal() {
    $('#ModalActualizarEditar').modal('hide');
    this.limpiarForm();
  }
  onCreateTipoDocumento() {
    const formValue = this.TipoDocumentoForm.value;

    if (this.isNew) {
      this.tipoDocumentoService.CreateTipoDocumento({ nombre: formValue.nombre, estado: Number(formValue.estado) }).subscribe(async res => {
        await this.listarTipoDocumento();
      });
    } else {
      this.tipoDocumentoService.UpdateTipoDocumento({ id: formValue.id, nombre: formValue.nombre, estado: Number(formValue.estado) }
      ).subscribe(async res => {
        await this.listarTipoDocumento();
      });
    }
    $('#ModalActualizarEditar').modal('hide');
    this.limpiarForm();
  }
  limpiarForm() {
    this.TipoDocumentoForm = this.fb.group({
      id: [0],
      nombre: [''],
      estado: [-1]
    });
  }

}

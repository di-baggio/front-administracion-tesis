import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { DxDataGridComponent } from 'devextreme-angular';
import { TipoSolicitudService } from './tipo-solicitud.service';
declare var $: any;
@Component({
  selector: 'app-tipo-solicitud',
  templateUrl: './tipo-solicitud.component.html',
  styleUrls: ['./tipo-solicitud.component.scss']
})
export class TipoSolicitudComponent implements OnInit {

  listTipoSolicitud: any[] = [];
  Estados: any[] = [{ estado: 0, nombre: 'INACTIVO' }, { estado: 1, nombre: 'ACTIVO' }];

  @ViewChild('gridTipoSolicitud', { static: true }) gridTipoSolicitud: DxDataGridComponent;
  isNew = false;
  TipoSolicitudForm = this.fb.group({
    id: [0],
    nombre: [''],
    estado: [-1]
  });
  constructor(private tipoSolicitudService: TipoSolicitudService, private fb: FormBuilder) { this.Editar = this.Editar.bind(this); }


  async ngOnInit() {
    await this.listarTipoSolicitud();
  }
  async listarTipoSolicitud() {
    await this.tipoSolicitudService.listarTipoSolicitud({}).subscribe(res => {
      this.listTipoSolicitud = res;
    });
  }
  Editar(e) {
    this.isNew = false;
    const data = e.row.data;
    this.TipoSolicitudForm.setValue({
      id: e.row.data.id,
      nombre: e.row.data.nombre,
      estado: e.row.data.estado
    });
    $('#ModalActualizarEditar').modal('show');
  }
  abrirModal() {
    this.isNew = true;
    $('#ModalActualizarEditar').modal('show');
  }
  cancelarModal() {
    $('#ModalActualizarEditar').modal('hide');
    this.limpiarForm();
  }
  onCreateTipoSolicitud() {
    const formValue = this.TipoSolicitudForm.value;

    if (this.isNew) {
      this.tipoSolicitudService.CreateTipoSolicitud({ nombre: formValue.nombre, estado: Number(formValue.estado) }).subscribe(async res => {
        await this.listarTipoSolicitud();
      });
    } else {
      this.tipoSolicitudService.UpdateTipoSolicitud({ id: formValue.id, nombre: formValue.nombre, estado: Number(formValue.estado) }
      ).subscribe(async res => {
        await this.listarTipoSolicitud();
      });
    }
    $('#ModalActualizarEditar').modal('hide');
    this.limpiarForm();
  }
  limpiarForm() {
    this.TipoSolicitudForm = this.fb.group({
      id: [0],
      nombre: [''],
      estado: [-1]
    });
  }

}

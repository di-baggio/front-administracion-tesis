import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { TipoSolicitud } from '../shared/models/tipo-solicitud.interface';

@Injectable({
  providedIn: 'root'
})
export class TipoSolicitudService {

  headersObject: HttpHeaders;
  httpOptions: any;
  constructor(private http: HttpClient, private toastr: ToastrService) {
    this.headersObject = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('token'));
    this.httpOptions = {
      headers: this.headersObject
    };
  }

  listarTipoSolicitud(data: any): Observable<TipoSolicitud[]> {

    return this.http.get(`${environment.APIURL}/TipoSolicitud${(data.id ? '?id=' + data.id : '')}`, this.httpOptions).pipe(
      map((res: any) => {
        const TipoSolicitudes: TipoSolicitud[] = res.data;
        return TipoSolicitudes;
      }),
      catchError((err: any) => this.handleError(err))
    );
  }

  UpdateTipoSolicitud(data: any): Observable<any> {

    return this.http.put(`${environment.APIURL}/TipoSolicitud/${data.id}`, data, this.httpOptions).pipe(
      map((res: any) => {
        this.toastr.success(res.message);
        return res;
      }),
      catchError((err: any) => this.handleError(err))
    );
  }
  CreateTipoSolicitud(data: any): Observable<any> {
    return this.http.post(`${environment.APIURL}/TipoSolicitud`, data, this.httpOptions).pipe(
      map((res: any) => {
        this.toastr.success(res.message);
        return res;
      }),
      catchError((err: any) => this.handleError(err))
    );
  }

  handleError(err: any): Observable<never> {
    if (err) {
      let errorMessage: any;
      if (typeof err.error.error === 'object') {
        errorMessage = err.error.error.message;
        this.toastr.error(err.error.error.message);
      } else {
        errorMessage = err.error.message;
        this.toastr.error(err.error.message);
      }
      return errorMessage;
    }
    return err;
  }
}

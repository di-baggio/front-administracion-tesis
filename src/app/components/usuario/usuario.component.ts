import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { DxDataGridComponent } from 'devextreme-angular';
import { TipoDocumentoService } from '../tipo-documento/tipo-documento.service';
import { UsuarioService } from './usuario.service';
declare var $: any;
@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.scss']
})
export class UsuarioComponent implements OnInit {

  listUsuario: any[] = [];
  listTipoDocumento: any[] = [];

  Estados: any[] = [{ estado: 0, nombre: 'INACTIVO' }, { estado: 1, nombre: 'ACTIVO' }];

  @ViewChild('gridUsuario', { static: true }) gridUsuario: DxDataGridComponent;
  isNew = false;
  UsuarioForm = this.fb.group({
    id: [0],
    id_tercero: [0],
    id_tipo_documento: [1],
    documento: [''],
    nombre: [''],
    email: [''],
    password: ['']
  });
  constructor(private usuarioService: UsuarioService, private fb: FormBuilder, private tipoDocumentoService: TipoDocumentoService) {
    this.Editar = this.Editar.bind(this);
    this.Desactivar = this.Desactivar.bind(this);
  }

  async ngOnInit() {
    await this.listarUsuario();
    await this.listarTipoDocumento();
  }
  async listarUsuario() {
    await this.usuarioService.listarUsuario({}).subscribe(res => {
      this.listUsuario = res;
    });
  }
  async listarTipoDocumento() {
    await this.tipoDocumentoService.listarTipoDocumento({}).subscribe(res => {
      this.listTipoDocumento = res;
    });
  }
  Editar(e) {
    this.isNew = false;
    this.UsuarioForm.setValue({
      id: e.row.data.id,
      id_tercero: e.row.data.id_tercero,
      id_tipo_documento: e.row.data.id_tipo_documento,
      documento: e.row.data.documento,
      nombre: e.row.data.nombre,
      email: e.row.data.email,
      password: e.row.data.password
    });
    $('#ModalActualizarEditar').modal('show');
  }
  Desactivar(e) {
    let estado: number;
    if (e.row.data.estado === 1) {
      estado = 0;
    } else {
      estado = 1;
    }
    this.usuarioService.deactivateUsuario({ estado });
  }
  abrirModal() {
    this.isNew = true;
    $('#ModalActualizarEditar').modal('show');
  }
  cancelarModal() {
    $('#ModalActualizarEditar').modal('hide');
    this.limpiarForm();
  }
  onCreateUsuario() {
    const formValue = this.UsuarioForm.value;

    if (this.isNew) {
      this.usuarioService.CreateUsuario({
        id_tercero: formValue.id_tercero,
        id_tipo_documento: formValue.id_tipo_documento,
        documento: formValue.documento,
        nombre: formValue.nombre,
        email: formValue.email,
        password: formValue.password,
        estado: 1
      }).subscribe(async res => {
        await this.listarUsuario();
      });
    } else {
      this.usuarioService.UpdateUsuario({
        id: formValue.id,
        id_tercero: formValue.id_tercero,
        id_tipo_documento: formValue.id_tipo_documento,
        documento: formValue.documento,
        nombre: formValue.nombre,
        email: formValue.email,
        estado: 1
      }).subscribe(async res => {
        await this.listarUsuario();
      });
    }
    $('#ModalActualizarEditar').modal('hide');
    this.limpiarForm();
  }
  limpiarForm() {
    this.UsuarioForm = this.fb.group({
      id: [0],
      id_tercero: [0],
      id_tipo_documento: [1],
      documento: [''],
      nombre: [''],
      email: [''],
      password: ['']
    });
  }

}

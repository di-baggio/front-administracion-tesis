import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Usuario } from '../shared/models/usuario.interface';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  headersObject: HttpHeaders;
  httpOptions: any;
  constructor(private http: HttpClient, private toastr: ToastrService) {
    this.headersObject = new HttpHeaders().set('Authorization', 'Bearer ' + localStorage.getItem('token'));
    this.httpOptions = {
      headers: this.headersObject
    };
  }
  listarUsuario(data: any): Observable<Usuario[]> {
    return this.http.get(`${environment.APIURL}/usuario`, this.httpOptions).pipe(
      map((res: any) => {
        const usuario: Usuario[] = res.data;
        return usuario;
      }),
      catchError((err: any) => this.handleError(err))
    );
  }

  UpdateUsuario(data: any): Observable<any> {
    return this.http.put(`${environment.APIURL}/Usuario/${data.id}`, data, this.httpOptions).pipe(
      map((res: any) => {
        this.toastr.success(res.message);
        return res;
      }),
      catchError((err: any) => this.handleError(err))
    );
  }
  deactivateUsuario(data: any): Observable<any> {
    return this.http.put(`${environment.APIURL}/usuario/estado/${data.id}`, data, this.httpOptions).pipe(
      map((res: any) => {
        this.toastr.success(res.message);
        return res;
      }),
      catchError((err: any) => this.handleError(err))
    );
  }
  CreateUsuario(data: any): Observable<any> {
    return this.http.post(`${environment.APIURL}/Usuario`, data, this.httpOptions).pipe(
      map((res: any) => {
        this.toastr.success(res.message);
        return res;
      }),
      catchError((err: any) => this.handleError(err))
    );
  }

  handleError(err: any): Observable<never> {
    if (err) {
      let errorMessage: any;
      if (typeof err.error.error === 'object') {
        errorMessage = err.error.error.message;
        this.toastr.error(err.error.error.message);
      } else {
        errorMessage = err.error.message;
        this.toastr.error(err.error.message);
      }
      return errorMessage;
    }
    return err;
  }
}
